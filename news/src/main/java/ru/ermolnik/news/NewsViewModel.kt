package ru.ermolnik.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ru.mts.data.news.repository.NewsRepository
import ru.mts.data.utils.doOnError
import ru.mts.data.utils.doOnSuccess

class NewsViewModel(private val repository: NewsRepository) : ViewModel() {
    private val _state: MutableStateFlow<NewsState> = MutableStateFlow(NewsState.Loading)
    val state = _state.asStateFlow()

    init {
        getNews()
    }

    fun refreshNews() {
        viewModelScope.launch {
            _state.update { oldState ->
                when (oldState) {
                    is NewsState.Content -> oldState.copy(isRefreshing = true)
                    else -> oldState
                }
            }
            repository.fetch().collect {
                it.doOnError { error ->
                    _state.emit(NewsState.Error(error))
                }.doOnSuccess { news ->
                    _state.update { oldState ->
                        when (oldState) {
                            is NewsState.Content -> oldState.copy(isRefreshing = false, news)
                            else -> oldState
                        }
                    }
                }
            }
        }
    }

    fun getNews() {
        viewModelScope.launch {
            repository.getNews().collect {
                it.doOnError { error ->
                    _state.emit(NewsState.Error(error))
                }.doOnSuccess { news ->
                    _state.update { NewsState.Content(isRefreshing = false, news) }
                }
            }
        }
    }

    fun retry() {
        viewModelScope.launch {
            _state.update { NewsState.Loading }
            repository.getNews().collect {
                it.doOnError { error ->
                    _state.emit(NewsState.Error(error))
                }.doOnSuccess { news ->
                    _state.update { NewsState.Content(isRefreshing = false, news) }
                }
            }
        }
    }


}
