package ru.ermolnik.news

import ru.mts.data.news.repository.News

sealed interface NewsState {
    object Loading: NewsState
    data class Error(val throwable: Throwable): NewsState
    data class Content(val isRefreshing: Boolean, val news: List<News>) : NewsState
}