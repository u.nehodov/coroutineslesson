package ru.ermolnik.news

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import ru.mts.data.news.repository.News

@Composable
fun NewsScreen(viewModel: NewsViewModel) {
    val state by viewModel.state.collectAsState()
    ShowState(state = state, onRefresh = { viewModel.refreshNews() }, onTryAgainClick = { viewModel.retry() })
}

@Composable
fun ShowState(
    state: NewsState,
    onRefresh: () -> Unit,
    onTryAgainClick: () -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        when (state) {
            is NewsState.Loading -> {
                CircularProgressIndicator(
                    modifier = Modifier
                        .size(50.dp)
                        .align(Alignment.Center)
                )
            }
            is NewsState.Error -> {
                Text(
                    text = state.throwable.toString(),
                    modifier = Modifier
                        .wrapContentSize()
                        .align(Alignment.Center),
                    textAlign = TextAlign.Center
                )
                Button(
                    modifier = Modifier
                        .wrapContentSize()
                        .padding(80.dp)
                        .align(Alignment.BottomCenter),
                    onClick = { onTryAgainClick.invoke() }
                ) {
                    Text(text = "Try again")
                }
            }
            is NewsState.Content -> {
                Content(modifier = Modifier.align(Alignment.Center), content = state, onRefresh = onRefresh)
            }
        }
    }
}

@Composable
fun Content(
    modifier: Modifier,
    content: NewsState.Content,
    onRefresh: () -> Unit
) {
    SwipeRefresh(
        state = rememberSwipeRefreshState(isRefreshing = content.isRefreshing),
        onRefresh = { onRefresh.invoke() }
    ) {
        LazyColumn(
            modifier = modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(color = MaterialTheme.colors.background),
            horizontalAlignment = Alignment.CenterHorizontally,
            contentPadding = PaddingValues(8.dp)
        ) {
            items(content.news) {
                NewsItem(item = it)
            }
        }
    }
}

@Composable
fun NewsItem(item: News) {
    Card(
        modifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp),
        shape = MaterialTheme.shapes.large,
        elevation = 8.dp
    ) {
        Column {
            Text(
                text = item.title,
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = MaterialTheme.colors.secondary),
                textAlign = TextAlign.Center,
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold,
            )
            Divider(Modifier.fillMaxWidth())
            Text(
                text = item.content,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 4.dp, horizontal = 8.dp),
                textAlign = TextAlign.Start,
                fontSize = 16.sp,
                fontStyle = FontStyle.Italic
            )
        }
    }
}
