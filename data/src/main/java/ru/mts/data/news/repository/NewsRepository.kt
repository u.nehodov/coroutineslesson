package ru.mts.data.news.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import ru.mts.data.news.db.NewsLocalDataSource
import ru.mts.data.news.db.toDomain
import ru.mts.data.news.remote.NewsRemoteDataSource
import ru.mts.data.news.remote.toDomain
import ru.mts.data.news.remote.toEntity
import ru.mts.data.utils.Result
import ru.mts.data.utils.doOnError
import ru.mts.data.utils.doOnSuccess
import ru.mts.data.utils.mapSuccess

class NewsRepository(
    private val newsLocalDataSource: NewsLocalDataSource,
    private val newsRemoteDataSource: NewsRemoteDataSource,
) {

    suspend fun getNews(): Flow<Result<List<News>, Throwable>> {
        return flow {
            newsLocalDataSource.getNews().mapSuccess { newsFromDb ->
                if (newsFromDb.isEmpty()) {
                    newsRemoteDataSource.getNews()
                        .doOnSuccess { response ->
                            emit(Result.Success(response.news.map { it.toDomain() }))
                            newsLocalDataSource.insertAll(response.news.map { it.toEntity() })
                        }.doOnError {
                            emit(Result.Error(it))
                        }
                } else {
                    val result = newsLocalDataSource.getNews()
                        .mapSuccess { newsEntities ->
                            newsEntities.map { it.toDomain() }
                        }
                    emit(result)
                }
            }
        }
    }

    suspend fun fetch(): Flow<Result<List<News>, Throwable>> {
        return flow {
            val result = newsRemoteDataSource.getNews()
                .mapSuccess { response ->
                    newsLocalDataSource.insertAll(response.news.map { it.toEntity() })
                    response.news.map { it.toDomain() }
                }
            emit(result)
        }
    }

}
