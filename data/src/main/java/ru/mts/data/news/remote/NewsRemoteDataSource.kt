package ru.mts.data.news.remote

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import ru.mts.data.main.NetworkClient
import ru.mts.data.utils.runOperationCatching
import ru.mts.data.utils.Result

class NewsRemoteDataSource {
    suspend fun getNews(): Result<NewsDto.Response, Throwable> = withContext(Dispatchers.IO) {
        return@withContext runOperationCatching {
            delay(1000L)
            NetworkClient.create().getSampleData(NewsDto.Request(1))
        }
    }
}