package ru.mts.data.news.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface NewsDao {

    @Query("SELECT * FROM news WHERE id = :id")
    suspend fun getById(id: Long): NewsEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(news: NewsEntity?)

    @Insert
    suspend fun insertAll(news: List<NewsEntity?>)

    @Update
    suspend fun update(news: NewsEntity?)

    @Query("SELECT * FROM news")
    suspend fun getAll(): List<NewsEntity>

    @Delete
    suspend fun delete(news: NewsEntity?)

    @Query("DELETE FROM news")
    suspend fun clear()
}