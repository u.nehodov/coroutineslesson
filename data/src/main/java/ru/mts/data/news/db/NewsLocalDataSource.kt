package ru.mts.data.news.db

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import ru.mts.data.main.AppDatabase
import ru.mts.data.utils.Result
import ru.mts.data.utils.VoidResult
import ru.mts.data.utils.runOperationCatching

class NewsLocalDataSource(private val context: Context) {

    private val dao
        get() = AppDatabase.getDatabase(context).newsDao()

    suspend fun insertAll(news: List<NewsEntity>): VoidResult<Throwable> = runOperationCatching {
        withContext(Dispatchers.IO) { dao.insertAll(news) }
    }

    suspend fun getNews(): Result<List<NewsEntity>, Throwable> {
        return runOperationCatching {
            withContext(Dispatchers.IO) {
                dao.getAll()
            }
        }
    }

    suspend fun clear() = withContext(Dispatchers.IO) { dao.clear() }
}
