package ru.mts.data.news.remote

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import ru.mts.data.news.db.NewsEntity
import ru.mts.data.news.repository.News


class NewsDto {
    @Parcelize
    data class Request(@SerializedName("id") val id: Int) : Parcelable

    @Parcelize
    data class Response(
        @SerializedName("news") val news: List<NewsDtoItem>
    ) : Parcelable
}

@Parcelize
data class NewsDtoItem(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("content") val content: String,
): Parcelable

internal fun NewsDtoItem.toDomain(): News {
    return News(
        id = this.id,
        title = this.title,
        content = this.content
    )
}

internal fun NewsDtoItem.toEntity(): NewsEntity {
    return NewsEntity(
        id = this.id,
        title = this.title,
        content = this.content
    )
}