package ru.mts.data.news.repository

data class News(
    val id: Int,
    val title: String,
    val content: String,
)
